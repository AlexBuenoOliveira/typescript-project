"use strict";
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 2] = "ADMIN";
    Role[Role["READY_ONLY"] = 3] = "READY_ONLY";
    Role["AUTHOR"] = "author";
})(Role || (Role = {}));
;
const person = {
    name: "Alexandre",
    nickname: "Alex",
    age: 30,
    hobbies: ["Sports", "Cooking"],
    role: Role.ADMIN
};
let favouriteActivities;
favouriteActivities = ["Sports"];
console.log(person.age);
for (const hobby of person.hobbies) {
    console.log(hobby);
}
if (person.role === Role.ADMIN) {
    console.log(Role.ADMIN);
}
