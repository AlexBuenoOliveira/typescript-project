function add1(n1: number, n2: number) {
  return n1 + n2;
}

//void means that the function doesnt return anything
 function printResult1(num: number) {
   console.log("Result: " + num);
 }

function addAndHandle(n1: number, n2: number, cb: (num: number) => void){
    const result = n1+ n2;
    cb(result);
}

printResult1(add1(5, 12));

let combineValues: (a: number, b: number) => number;

combineValues = add1;
// combineValues = 5;

console.log(combineValues(8, 8));
// console.log(printResult('a'));

addAndHandle(10, 20, (result) => {
    console.log(result)
})
