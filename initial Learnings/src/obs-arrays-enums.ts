/*
const person: {
    name: string
    nickname: string
    age: number
    hobbies: string[]
    role: [number, string]//setting the Tuple
} = {
  name: "Alexandre",
  nickname: "Alex",
  age: 30,
  hobbies: ["Sports", "Cooking"],
  role: [2, 'author']
};
*/

enum Role { ADMIN = 2, READY_ONLY, AUTHOR = 'author' };

const person = {
  name: "Alexandre",
  nickname: "Alex",
  age: 30,
  hobbies: ["Sports", "Cooking"],
  role: Role.ADMIN
};

//person.role.push("admin");
//person.role[1] = 10

let favouriteActivities: string[];
favouriteActivities = ["Sports"];

console.log(person.age);

for (const hobby of person.hobbies) {
  console.log(hobby);
}

if (person.role === Role.ADMIN) {
    console.log(Role.ADMIN);
}
